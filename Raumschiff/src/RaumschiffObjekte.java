import java.util.ArrayList;

public class RaumschiffObjekte {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int lebenserhaltungsystemInProzent;
	private int androidenNazahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<LadungObjekte> landungsverzeichnis = new ArrayList<LadungObjekte>();
	
	public RaumschiffObjekte(int photonentorpedoAnzahl, int energieversorgungInPrizent, int schildeInProzent,
			int lebenserhaltungsystemInProzent, int androidenNazahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInPrizent;
		this.schildeInProzent = schildeInProzent;
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
		this.androidenNazahl = androidenNazahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.landungsverzeichnis = new ArrayList<LadungObjekte>();
	}
	
	public RaumschiffObjekte(int i, int j, int k, int l, int m, String string, ArrayList<LadungObjekte> arrayList) {
	}
	
	/** 
	 * @return int
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	/** 
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	/** 
	 * @return int
	 */
	public int getEnergieversorgungInPrizent() {
		return energieversorgungInProzent;
	}
	
	/** 
	 * @param energieversorgungInPrizent
	 */
	public void setEnergieversorgungInPrizent(int energieversorgungInPrizent) {
		this.energieversorgungInProzent = energieversorgungInPrizent;
	}

	/** 
	 * @return int
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	/** 
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	/** 
	 * @return int
	 */
	public int getLebenserhaltungsystemInProzent() {
		return lebenserhaltungsystemInProzent;
	}
	
	/** 
	 * @param lebenserhaltungsystemInProzent
	 */
	public void setLebenserhaltungsystemInProzent(int lebenserhaltungsystemInProzent) {
		this.lebenserhaltungsystemInProzent = lebenserhaltungsystemInProzent;
	}
	
	/** 
	 * @return int
	 */
	public int getAndroidenNazahl() {
		return androidenNazahl;
	}
	
	/** 
	 * @param androidenNazahl
	 */
	public void setAndroidenNazahl(int androidenNazahl) {
		this.androidenNazahl = androidenNazahl;
	}
	
	/** 
	 * @return String
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	
	/** 
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/** 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	/** 
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	/** 
	 * @return ArrayList<LadungObjekte>
	 */
	public ArrayList<LadungObjekte> getLandungsverzeichnis() {
		return landungsverzeichnis;
	}	
	
	/** 
	 * @param landungsverzeichnis
	 */
	public void setLandungsverzeichnis(ArrayList<LadungObjekte> landungsverzeichnis) {
		this.landungsverzeichnis = landungsverzeichnis;
	}

	/** 
	 * @return String
	 * alle getter und setter werden hier im Terminal ausgegeben
	 */
	@Override
	public String toString() {
		return "Androiden Azahl = " + androidenNazahl + ", \nBroadcastkommunikator = "
				+ broadcastKommunikator + ", \nEnergieversorgung = " + energieversorgungInProzent + "%"
				+ ", \nLandungsverzeichnis = " + landungsverzeichnis + ", \nLebenserhaltungsystem = "
				+ lebenserhaltungsystemInProzent + "%" + ", \nphotonentorpedoAnzahl = " + photonentorpedoAnzahl
				+  ", \nSchiffsname = " + schiffsname + ", \nSchild = "
				+ schildeInProzent + "%";
	}		
}