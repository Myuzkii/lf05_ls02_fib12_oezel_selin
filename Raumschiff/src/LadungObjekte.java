public class LadungObjekte {
	private String bezeichnung;
	private int menge;

	public LadungObjekte() {
		System.out.println("Standart");
	}

	public LadungObjekte(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	} 

	public int getMenge() {
		return menge;
	}
	
	public void setMenge(int menge) {
		this.menge = menge;
	}

	/** 
	 * @return String
	 */
	@Override
	public String toString() {
		return "[Bezeichnung = " + bezeichnung + "\n" +", Menge = " + menge + "]";
	}
	
	LadungObjekte lo1 = new LadungObjekte("Plasma Waffe", 50);
	LadungObjekte lo2 = new LadungObjekte("Photonentorpedo", 3);
	LadungObjekte lo3 = new LadungObjekte("Klingonen Schwert", 200);
	LadungObjekte lo4 = new LadungObjekte("Ferengi", 200);
	LadungObjekte lo5 = new LadungObjekte("Rote Materie", 2);
	LadungObjekte lo6 = new LadungObjekte("Forschungsonde", 35);
	LadungObjekte lo7 = new LadungObjekte("Borg-Schrott", 5);
}
